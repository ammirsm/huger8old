/**
 * @author Amir
 */
function index(a){
		if(a==1){
		$("div#logotype").delay(100).fadeTo('slow',1);
		$('div#ContactUsIn').delay(200).fadeTo('slow',1);
   		$('div#AboutUs').delay(300).fadeTo('slow',1);
  		$('div#HistoryIn').delay(400).fadeTo('slow',1);
        $('div#MediaIn').delay(500).fadeTo('slow',1);
		$('div#BlogIn').delay(600).fadeTo('slow',1);
		$("div#mainbox").delay(700).css("display","block");
		}
		else{
		$("div#logotype").delay(100).fadeTo('slow',0);
		$('div#ContactUsIn').delay(200).fadeTo('slow',0);
   		$('div#AboutUs').delay(300).fadeTo('slow',0);
  		$('div#HistoryIn').delay(400).fadeTo('slow',0);
        $('div#MediaIn').delay(500).fadeTo('slow',0);
		$('div#BlogIn').delay(600).fadeTo('slow',0);
		$("div#mainbox").css("display","none");
		}
}
$(document).ready(function(){
	
		$("div#logotype").fadeTo('fast',0);
		$('div#ContactUsIn').fadeTo('fast',0);
   		$('div#AboutUs').fadeTo('fast',0);
  		$('div#HistoryIn').fadeTo('fast',0);
        $('div#MediaIn').fadeTo('fast',0);
		$('div#BlogIn').fadeTo('fast',0);
		$("div#logotype").delay(100).fadeTo('slow',1);
		$('div#ContactUsIn').delay(200).fadeTo('slow',1);
   		$('div#AboutUs').delay(300).fadeTo('slow',1);
  		$('div#HistoryIn').delay(400).fadeTo('slow',1);
        $('div#MediaIn').delay(500).fadeTo('slow',1);
		$('div#BlogIn').delay(600).fadeTo('slow',1);
		
	$("div#AboutUs").click(function(){
			index(0);
			$("div#TeamBox").fadeTo('fast',0);
			$("div#TeamBox").css("display","block");
			$("div#TeamBox").fadeTo('slow',1);
	});
	$("div#BackAbout").click(function(){
			index(1);
			
			$("div#AboutBox").css("display","none");
			
	});
	$("div#ContactUsIn").click(function(){
			index(0);
			$("div#contactus").fadeTo('fast',0);
			$("div#contactus").css("display","block");
			$("div#contactus").fadeTo('slow',1);
	});
	$("div#BackContact").click(function(){
			index(1);
			
			$("div#contactus").css("display","none");
			
	});
	$("div#HistoryIn").click(function(){
			index(0);
			$("div#AboutBox").fadeTo('fast',0);
			$("div#AboutBox").css("display","block");
			$("div#AboutBox").fadeTo('slow',1);
			
	});
	$("div#TeamBack").click(function(){
			index(1);
			
			$("div#TeamBox").css("display","none");
			
	});
	
	
});
